$(document).ready(function() {

    $("#lima").on("click", function() {
        window.open("https://www.lima-city.de/?cref=394024");
    });

    $("#netcup").on("click", function() {
        window.open("https://www.netcup.de/bestellen/gutschein_einloesen.php?gutschein=36nc15890610640");
    });

    $(document).on("scroll", function() {
        button = document.getElementById("scrolltop");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            button.style.display = "block";
        } else {
            button.style.display = "none";
        }    
    });

    $("#scrolltop").on("click", function() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    });
});